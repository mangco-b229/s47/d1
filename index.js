
// DISCUSSION #1

const  txtFirsName = document.querySelector('#txt-firstName')
const  txtLastName = document.querySelector('#txt-lastName')
const  spanFullName = document.querySelector('#span-full-name')

// Using "keyUp" event

/*txtFirsName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirsName.value;
})*/

// DISCUSSION #2

const updateFullname = () => {
	let firstName = txtFirsName.value
	let lastName = txtLastName.value

	spanFullName.innerHTML = `${firstName} ${lastName}`
}

txtFirsName.addEventListener('keyup', updateFullname)
txtLastName.addEventListener('keyup', updateFullname)


